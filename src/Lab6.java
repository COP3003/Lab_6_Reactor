/*
 * FileName: Lab6.java
 * Author: 5153
 * Created: 13 December, 2017
 * Last Modified: 13 December, 2017
 */
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Simulates a simple Hydrogen and Oxygen reaction to produce water molecules.
 *  Demonstrates Java multi-threading.
 */
public class Lab6 {
  public static void main(String[] args) {
    // synchronized buffer shared by hydrogen, oxygen and reactor threads
    ReactionArea buff = new ReactionArea();

    // Create new pool with three threads.
    ExecutorService executorService = Executors.newCachedThreadPool();

    // Execute the objects with access to the shared buffer
    executorService.execute(new Hydrogen(buff));
    executorService.execute(new Oxygen(buff));
    executorService.execute(new Reactor(buff));

    executorService.shutdown(); //terminate app when tasks are complete
  }

  /**
   * Buffer class for consuming hydrogen and oxygen and producing water.
   */
  static class ReactionArea{
    int waitingHydrogen=0; // number of un-reacted hydrogens
    int waitingOxygen=0;  // number of un-reacted oxygens

    /**
     * Adds a Hydrogen atom to the reaction area. If 6 hydrogen atoms are
     *  waiting, the requesting thread is placed in the wait cue until the
     *  count is below 6;
     * @param index int the nth atom added
     * @throws InterruptedException something evil this way comes
     */
    synchronized void increWHydrogen(int index) throws InterruptedException {
      while (waitingHydrogen > 5) { // waiting area until less than 6 atoms
        wait();
      }
      waitingHydrogen++;  // put an atom in the reaction area
      System.out.printf("The %dth hydrogen atom was added.\n",index);
      notifyAll();  // let the other threads know the buffer is unlocked
    }

    /**
     * Adds an Oxygen atom to the reaction area. If 3 oxygen atoms are
     *  waiting, the requesting thread is placed in the wait cue until the
     *  count is below 2;
     * @param index int the nth atom added
     * @throws InterruptedException something evil this way comes
     */
    synchronized void increWOxygen(int index) throws InterruptedException {
      while (waitingOxygen > 2) { // waiting area until less than 2 atoms
        wait();
      }
      waitingOxygen++;  // put an atom in the reaction area
      System.out.printf("The %dth Oxygen atom was added.\n",index);
      notifyAll();  // let the other threads know the buffer is unlocked
    }

    /**
     * Removes two Hydrogen atoms and one Oxygen atom from the reaction area
     *  (creates a water molecule). If not enough atoms available the thread
     *  waits until another one is added to the reaction area (buffer).
     * @param index int number of water molecules created
     * @throws InterruptedException something evil this way comes
     */
    synchronized void react(int index) throws InterruptedException {
      // wait until at least 1 oxygen and 2 hydrogen in the reaction area
      while (waitingOxygen == 0 || waitingHydrogen < 2) {
        wait();
      }
      waitingHydrogen -= 2; // remove two hydrogen atoms
      waitingOxygen--;  // remove one oxygen atom
      System.out.printf("The %dth water molecule was formed.\n",index);
      notifyAll();  // let the other threads know the buffer is unlocked
      Thread.sleep(50); // wait 50 milliseconds before trying again
    }
  }

  /**
   *  (Producer) supplies hydrogen atoms to the reaction area.
   */
  static class Hydrogen implements Runnable{
    ReactionArea buff;  // shared buffer

    // constructor
    Hydrogen(ReactionArea buff){
      this.buff=buff;
    }

    /**
     * Add 20 hydrogen atoms to the reaction chamber one at a time. Sleep 100
     *  milliseconds after each insertion.
     */
    public void run(){
      for(int i=0;i<20;i++) {
        try {
          buff.increWHydrogen(i);
          Thread.sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    }
  }

  /**
   * (Producer) supplies oxygen atoms to the reaction area.
   */
  static class Oxygen implements Runnable{
    ReactionArea buff;  // shared buffer

    // constructor
    Oxygen(ReactionArea buff){
      this.buff=buff;
    }

    /**
     * Add 10 oxygen atoms to the reaction chamber one at a time. Sleep 200
     *  milliseconds after each insertion.
     */
    public void run(){
      for(int i=0;i<10;i++) {
        try {
          buff.increWOxygen(i);
          Thread.sleep(200);
        } catch (InterruptedException e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    }
  }

  /**
   * (Consumer) creates water molecules from hydrogen and oxygen atoms.
   */
  static class Reactor implements Runnable{
    ReactionArea buff;  // shared buffer

    // constructor
    Reactor(ReactionArea buff){
      this.buff=buff;
    }

    /**
     * Create ten water molecules one at a time by removing two hydrogen atoms
     *  and one oxygen atom from the reaction area for each water molecule.
     *  Sleep 50 milliseconds after each water molecule created.
     */
    public void run(){
      try {
        Thread.sleep(2000); // wait 2 seconds before creating water
      } catch (InterruptedException e) {
        e.printStackTrace();
        System.exit(1);
      }
      for(int i=0;i<10;i++) {
        try {
          buff.react(i);
          Thread.sleep(50);
        } catch (InterruptedException e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    }
  }
}

